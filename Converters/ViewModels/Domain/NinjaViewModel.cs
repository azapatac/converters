﻿namespace Converters.ViewModels
{
    using System.Windows.Input;

    public class NinjaViewModel : BaseViewModel
    {
        public ICommand ShowNameCommand { get; set; }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        bool master;
        public bool Master
        {
            get
            {
                return master;
            }
            set
            {
                if (value != master)
                {
                    master = value;
                    OnPropertyChanged();
                }
            }
        }


        public NinjaViewModel()
        {

        }
    }
}