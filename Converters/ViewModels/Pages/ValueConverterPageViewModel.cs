﻿namespace Converters.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using global::Converters.Data;

    public class ValueConverterPageViewModel : BaseViewModel
    {
        IList<NinjaViewModel> ninjas;
        public IList<NinjaViewModel> Ninjas
        {
            get
            {
                return ninjas;
            }
            set
            {
                if (value != ninjas)
                {
                    ninjas = value;
                    OnPropertyChanged();
                }
            }
        }

        public ValueConverterPageViewModel()
        {            
            Ninjas = new ObservableCollection<NinjaViewModel>();
            Title = "Ninjas Page";

            var _ninjas = NinjaDataBase.LoadNinjas();

            _ninjas.ForEach((ninja) =>
            {
                Ninjas.Add(new NinjaViewModel
                {
                    Master = ninja.Master,
                    Name = ninja.Name
                });
            });
        }
    }
}