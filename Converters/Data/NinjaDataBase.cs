﻿namespace Converters.Data
{
    using System.Collections.Generic;
    using global::Converters.Models;

    public static class NinjaDataBase
    {
        public static List<Ninja> Ninjas = new List<Ninja>
        {
            { new Ninja { Name = "Fujibayashi", Master = true } },
            { new Ninja { Name = "Hattori" } },
            { new Ninja { Name = "Ishikawa" } },
            { new Ninja { Name = "Mochizuki", Master = true } },
            { new Ninja { Name = "Momochi", Master = true } },
        };

        public static List<Ninja> LoadNinjas()
        {
            return Ninjas;
        }
    }
}