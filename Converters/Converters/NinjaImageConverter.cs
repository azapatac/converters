﻿namespace Converters.Converters
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using Xamarin.Forms;

    public class NinjaImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return value;
            }

            var source = (bool)value ? "Converters.Resources.Images.blueninja.png" : "Converters.Resources.Images.orangeninja.png";

            var imageSource = ImageSource.FromResource(source, typeof(NinjaImageConverter).GetTypeInfo().Assembly);

            return imageSource; ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value; 
        }
    }
}