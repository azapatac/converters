﻿namespace Converters.Converters
{
    using System;
    using System.Globalization;
    using Xamarin.Forms;

    public class NinjaDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return value;
            }

            return (bool)value ? "Master" : "Student";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}