﻿using System;

namespace Converters.Converters
{
    using System.Globalization;
    using Xamarin.Forms;

    public class StatusConverter<T> : IValueConverter
    {
        public T Active { get; set; }
        public T Inactive { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return value;
            }

            return (bool)value ? Active : Inactive;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
