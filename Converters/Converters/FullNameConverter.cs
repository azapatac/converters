﻿namespace Converters.Converters
{
    using System;
    using System.Globalization;
    using Xamarin.Forms;

    public class FullNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
            {
                return values;
            }

            var fullName = string.Empty;

            foreach (var value in values)
            {
                fullName += $"{value} ";
            }

            return fullName;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}