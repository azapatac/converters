﻿namespace Converters.Models
{
    public class Ninja
    {
        public string Name { get; set; }
        public bool Master { get; set; }
    }
}